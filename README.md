# Yo, Dont Propagate Entries For Multi-Site Structures plugin for Craft CMS 3.x

Hacks Section settings for Structures to enable non-propagating Structures for multi-site installs. I'm thirsty

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require mmikkel/yo,-dont-propagate-entries-for-multi-site-structures

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Yo, Dont Propagate Entries For Multi-Site Structures.

## Yo, Dont Propagate Entries For Multi-Site Structures Overview

-Insert text here-

## Configuring Yo, Dont Propagate Entries For Multi-Site Structures

-Insert text here-

## Using Yo, Dont Propagate Entries For Multi-Site Structures

-Insert text here-

## Yo, Dont Propagate Entries For Multi-Site Structures Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Mats Mikkel Rummelhoff](https://vaersaagod.no)
