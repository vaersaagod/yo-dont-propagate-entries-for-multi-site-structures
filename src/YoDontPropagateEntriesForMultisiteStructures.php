<?php
/**
 * Yo, Dont Propagate Entries For Multi-Site Structures plugin for Craft CMS 3.x
 *
 * Hacks Section settings for Structures to enable non-propagating Structures for multi-site installs. I'm thirsty
 *
 * @link      https://vaersaagod.no
 * @copyright Copyright (c) 2018 Mats Mikkel Rummelhoff
 */

namespace mmikkel\yodontpropagateentriesformultisitestructures;

use mmikkel\yodontpropagateentriesformultisitestructures\YoDontPropagateEntriesForMultisiteStructuresBundle as Bundle;

use Craft;
use craft\services\Plugins;
use craft\services\Sections;
use craft\web\View;

use yii\base\Event;
use yii\base\Module;

/**
 * Class YoDontPropagateEntriesForMultisiteStructures
 *
 * @author    Mats Mikkel Rummelhoff
 * @package   YoDontPropagateEntriesForMultisiteStructures
 * @since     1.0.0
 *
 * @property 
 */
class YoDontPropagateEntriesForMultisiteStructures extends Module
{
    // Static Properties
    // =========================================================================

    /**
     * @var YoDontPropagateEntriesForMultisiteStructures
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        if (!Craft::$app->getRequest()->getIsCpRequest() || Craft::$app->getRequest()->getIsConsoleRequest()) {
            return;
        }

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_LOAD_PLUGINS,
            function (Event $event) {
                $this->getBusy();
            }
        );
    }

    // Protected Methods
    // =========================================================================
    protected function getBusy()
    {
        // Register alias
        Craft::setAlias('@mmikkel/yodontpropagateentriesformultisitestructures', __DIR__);

        Event::on(
            View::class,
            View::EVENT_BEFORE_RENDER_TEMPLATE,
            function () {
                try {
                    Craft::$app->getView()->registerAssetBundle(Bundle::class);
                } catch (InvalidConfigException $e) {
                    Craft::error(
                        'Error registering AssetBundle - ' . $e->getMessage(),
                        __METHOD__
                    );
                }
            }
        );
    }

}
