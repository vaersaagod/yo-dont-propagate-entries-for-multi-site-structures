<?php
/**
 * Created by PhpStorm.
 * User: mmikkel
 * Date: 18/05/2018
 * Time: 17:46
 */

namespace mmikkel\yodontpropagateentriesformultisitestructures;

use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

class YoDontPropagateEntriesForMultisiteStructuresBundle extends AssetBundle
{
    public function init()
    {
        // define the path that your publishable resources live
        $this->sourcePath = '@mmikkel/yodontpropagateentriesformultisitestructures/resources';

        // define the dependencies
        $this->depends = [
            CpAsset::class,
        ];

        // define the relative path to CSS/JS files that should be registered with the page
        // when this asset bundle is registered
        $this->js = [
            'punkrock.js',
        ];

        parent::init();
    }
}
