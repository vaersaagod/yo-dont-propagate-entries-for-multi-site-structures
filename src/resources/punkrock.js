$(function () {

    if (!window.Craft || !$('input[name="action"][value="sections/save-section"]').length) {
        return;
    }

    var types = ['structure', 'channel'];
    var $typeSelect = $('select#type');

    function moveField(type) {
        if (types.indexOf(type) === -1) {
            return;
        }
        $('#propagateEntries-field').appendTo($('#content').first('.type-' + type));
    }

    $typeSelect.on('change', function () {
        moveField($(this).val());
    });

    moveField($typeSelect.val());

});
