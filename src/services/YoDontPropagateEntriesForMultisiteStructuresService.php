<?php
/**
 * Created by PhpStorm.
 * User: mmikkel
 * Date: 18/05/2018
 * Time: 21:32
 */

namespace mmikkel\yodontpropagateentriesformultisitestructures\services;

use Craft;
use craft\base\Element;
use craft\db\Query;
use craft\elements\Entry;
use craft\errors\EntryTypeNotFoundException;
use craft\errors\SectionNotFoundException;
use craft\events\EntryTypeEvent;
use craft\events\SectionEvent;
use craft\helpers\ArrayHelper;
use craft\models\EntryType;
use craft\models\Section;
use craft\models\Section_SiteSettings;
use craft\models\Structure;
use craft\services\Sections;
use craft\queue\jobs\ResaveElements;
use craft\records\EntryType as EntryTypeRecord;
use craft\records\Section as SectionRecord;
use craft\records\Section_SiteSettings as Section_SiteSettingsRecord;
use yii\base\Component;
use yii\base\Exception;

/**
 * Sections service.
 * An instance of the Sections service is globally accessible in Craft via [[\craft\base\ApplicationTrait::getSections()|<code>Craft::$app->sections</code>]].
 *
 * @author Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @since 3.0
 */
class YoDontPropagateEntriesForMultisiteStructuresService extends Sections
{

    /**
     * @var
     */
    private $_allSectionIds;

    /**
     * @var
     */
    private $_editableSectionIds;

    /**
     * @var
     */
    private $_sectionsById;

    /**
     * @var bool
     */
    private $_fetchedAllSections = false;

    /**
     * @var
     */
    private $_entryTypesById;

    /**
     * Saves a section.
     *
     * ```php
     * use craft\models\Section;
     * use craft\models\Section_SiteSettings;
     * $section = new Section([
     *     'name' => 'News',
     *     'handle' => 'news',
     *     'type' => Section::TYPE_CHANNEL,
     *     'siteSettings' => [
     *         new Section_SiteSettings([
     *             'siteId' => Craft::$app->sites->getPrimarySite()->id,
     *             'enabledByDefault' => true,
     *             'hasUrls' => true,
     *             'uriFormat' => 'foo/{slug}',
     *             'template' => 'foo/_entry',
     *         ]),
     *     ]
     * ]);
     * $success = Craft::$app->sections->saveSection($section);
     * ```
     *
     * @param Section $section The section to be saved
     * @param bool $runValidation Whether the section should be validated
     * @return bool
     * @throws SectionNotFoundException if $section->id is invalid
     * @throws \Throwable if reasons
     */
    public function saveSection(Section $section, bool $runValidation = true): bool
    {
        $isNewSection = !$section->id;

        // Fire a 'beforeSaveEntryType' event
        if ($this->hasEventHandlers(self::EVENT_BEFORE_SAVE_ENTRY_TYPE)) {
            $this->trigger(self::EVENT_BEFORE_SAVE_ENTRY_TYPE, new EntryTypeEvent([
                'entryType' => $entryType,
                'isNew' => $isNewEntryType,
            ]));
        }

        if ($runValidation && !$section->validate()) {
            Craft::info('Section not saved due to validation error.', __METHOD__);
            return false;
        }

        if (!$isNewSection) {
            $sectionRecord = SectionRecord::find()
                ->where(['id' => $section->id])
                ->with('structure')
                ->one();

            if (!$sectionRecord) {
                throw new SectionNotFoundException("No section exists with the ID '{$section->id}'");
            }

            $oldSection = new Section($sectionRecord->toArray([
                'id',
                'structureId',
                'name',
                'handle',
                'type',
                'enableVersioning',
                'propagateEntries',
            ]));
        } else {
            $sectionRecord = new SectionRecord();
        }

        // Main section settings
        if ($section->type === Section::TYPE_SINGLE) {
            $section->propagateEntries = true;
        }

        /** @var SectionRecord $sectionRecord */
        $sectionRecord->name = $section->name;
        $sectionRecord->handle = $section->handle;
        $sectionRecord->type = $section->type;
        $sectionRecord->enableVersioning = (bool)$section->enableVersioning;
        $sectionRecord->propagateEntries = (bool)$section->propagateEntries;

        // Get the site settings
        $allSiteSettings = $section->getSiteSettings();

        if (empty($allSiteSettings)) {
            throw new Exception('Tried to save a section without any site settings');
        }

        $db = Craft::$app->getDb();
        $transaction = $db->beginTransaction();

        try {
            // Do we need to create a structure?
            if ($section->type === Section::TYPE_STRUCTURE) {
                /** @noinspection PhpUndefinedVariableInspection */
                if (!$isNewSection && $oldSection->type === Section::TYPE_STRUCTURE) {
                    $structure = Craft::$app->getStructures()->getStructureById($oldSection->structureId);
                    $isNewStructure = false;
                } else {
                    $structure = new Structure();
                    $isNewStructure = true;
                }

                // If they've set maxLevels to 0 (don't ask why), then pretend like there are none.
                if ((int)$section->maxLevels === 0) {
                    $section->maxLevels = null;
                }

                $structure->maxLevels = $section->maxLevels;
                Craft::$app->getStructures()->saveStructure($structure);

                $sectionRecord->structureId = $structure->id;
                $section->structureId = $structure->id;
            } else {
                /** @noinspection PhpUndefinedVariableInspection */
                if (!$isNewSection && $oldSection->structureId) {
                    // Delete the old one
                    Craft::$app->getStructures()->deleteStructureById($oldSection->structureId);
                    $sectionRecord->structureId = null;
                }
            }

            $sectionRecord->save(false);

            // Now that we have a section ID, save it on the model
            if ($isNewSection) {
                $section->id = $sectionRecord->id;
            }

            // Might as well update our cache of the section while we have it. (It's possible that the URL format
            //includes {section.handle} or something...)
            $this->_sectionsById[$section->id] = $section;

            // Update the site settings
            // -----------------------------------------------------------------

            if (!$isNewSection) {
                // Get the old section site settings
                $allOldSiteSettingsRecords = Section_SiteSettingsRecord::find()
                    ->where(['sectionId' => $section->id])
                    ->indexBy('siteId')
                    ->all();
            } else {
                $allOldSiteSettingsRecords = [];
            }

            foreach ($allSiteSettings as $siteId => $siteSettings) {
                // Was this already selected?
                if (!$isNewSection && isset($allOldSiteSettingsRecords[$siteId])) {
                    $siteSettingsRecord = $allOldSiteSettingsRecords[$siteId];
                } else {
                    $siteSettingsRecord = new Section_SiteSettingsRecord();
                    $siteSettingsRecord->sectionId = $section->id;
                    $siteSettingsRecord->siteId = $siteId;
                }

                $siteSettingsRecord->enabledByDefault = $siteSettings->enabledByDefault;

                if ($siteSettingsRecord->hasUrls = $siteSettings->hasUrls) {
                    $siteSettingsRecord->uriFormat = $siteSettings->uriFormat;
                    $siteSettingsRecord->template = $siteSettings->template;
                } else {
                    $siteSettingsRecord->uriFormat = $siteSettings->uriFormat = null;
                    $siteSettingsRecord->template = $siteSettings->template = null;
                }

                $siteSettingsRecord->save(false);

                // Set the ID on the model
                $siteSettings->id = $siteSettingsRecord->id;
            }

            if (!$isNewSection) {
                // Drop any sites that are no longer being used, as well as the associated entry/element site
                // rows
                $siteIds = array_keys($allSiteSettings);

                /** @noinspection PhpUndefinedVariableInspection */
                foreach ($allOldSiteSettingsRecords as $siteId => $siteSettingsRecord) {
                    if (!in_array($siteId, $siteIds, false)) {
                        $siteSettingsRecord->delete();
                    }
                }
            }

            // Make sure there's at least one entry type for this section
            // -----------------------------------------------------------------

            if (!$isNewSection) {
                $entryTypeExists = (new Query())
                    ->select(['id'])
                    ->from(['{{%entrytypes}}'])
                    ->where(['sectionId' => $section->id])
                    ->exists();
            } else {
                $entryTypeExists = false;
            }

            if (!$entryTypeExists) {
                $entryType = new EntryType();
                $entryType->sectionId = $section->id;
                $entryType->name = $section->name;
                $entryType->handle = $section->handle;

                if ($section->type === Section::TYPE_SINGLE) {
                    $entryType->hasTitleField = false;
                    $entryType->titleLabel = null;
                    $entryType->titleFormat = '{section.name|raw}';
                } else {
                    $entryType->hasTitleField = true;
                    $entryType->titleLabel = Craft::t('app', 'Title');
                    $entryType->titleFormat = null;
                }

                $this->saveEntryType($entryType);
            }

            // Now, regardless of whether the section type changed or not, let the section type make sure
            // everything is cool
            // -----------------------------------------------------------------

            switch ($section->type) {
                case Section::TYPE_SINGLE:
                    $this->_onSaveSingle($section, $isNewSection, $allSiteSettings);
                    break;
                case Section::TYPE_STRUCTURE:
                    /** @noinspection PhpUndefinedVariableInspection */
                    $this->_onSaveStructure($section, $isNewSection, $isNewStructure, $allOldSiteSettingsRecords);
                    break;
            }

            // Finally, deal with the existing entries...
            // -----------------------------------------------------------------

            if (!$isNewSection) {
                if ($section->propagateEntries) {
                    // Find a site that the section was already enabled in, and still is
                    $oldSiteIds = array_keys($allOldSiteSettingsRecords);
                    $newSiteIds = array_keys($allSiteSettings);
                    $persistentSiteIds = array_values(array_intersect($newSiteIds, $oldSiteIds));

                    // Try to make that the primary site, if it's in the list
                    $siteId = Craft::$app->getSites()->getPrimarySite()->id;
                    if (!in_array($siteId, $persistentSiteIds, false)) {
                        $siteId = $persistentSiteIds[0];
                    }

                    Craft::$app->getQueue()->push(new ResaveElements([
                        'description' => Craft::t('app', 'Resaving {section} entries', [
                            'section' => $section->name,
                        ]),
                        'elementType' => Entry::class,
                        'criteria' => [
                            'siteId' => $siteId,
                            'sectionId' => $section->id,
                            'status' => null,
                            'enabledForSite' => false,
                        ]
                    ]));
                } else {
                    // Resave entries for each site
                    foreach ($allSiteSettings as $siteId => $siteSettings) {
                        Craft::$app->getQueue()->push(new ResaveElements([
                            'description' => Craft::t('app', 'Resaving {section} entries ({site})', [
                                'section' => $section->name,
                                'site' => $siteSettings->getSite()->name,
                            ]),
                            'elementType' => Entry::class,
                            'criteria' => [
                                'siteId' => $siteId,
                                'sectionId' => $section->id,
                                'status' => null,
                                'enabledForSite' => false,
                            ]
                        ]));
                    }
                }
            }

            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();

            throw $e;
        }

        // Fire an 'afterSaveSection' event
        if ($this->hasEventHandlers(self::EVENT_AFTER_SAVE_SECTION)) {
            $this->trigger(self::EVENT_AFTER_SAVE_SECTION, new SectionEvent([
                'section' => $section,
                'isNew' => $isNewSection
            ]));
        }

        return true;
    }

    // Private Methods
    // =========================================================================

    /**
     * Returns a Query object prepped for retrieving sections.
     *
     * @return Query
     */
    private function _createSectionQuery(): Query
    {
        return (new Query())
            ->select([
                'sections.id',
                'sections.structureId',
                'sections.name',
                'sections.handle',
                'sections.type',
                'sections.enableVersioning',
                'sections.propagateEntries',
                'structures.maxLevels',
            ])
            ->leftJoin('{{%structures}} structures', '[[structures.id]] = [[sections.structureId]]')
            ->from(['{{%sections}} sections'])
            ->orderBy(['name' => SORT_ASC]);
    }

    /**
     * Performs some Single-specific tasks when a section is saved.
     *
     * @param Section $section
     * @param bool $isNewSection
     * @param Section_SiteSettings[] $allSiteSettings
     * @see saveSection()
     * @throws Exception if reasons
     */
    private function _onSaveSingle(Section $section, bool $isNewSection, array $allSiteSettings)
    {
        // Get all the entries that currently exist for this section
        // ---------------------------------------------------------------------

        if (!$isNewSection) {
            $entryData = (new Query())
                ->select([
                    'e.id',
                    'typeId',
                    'siteId' => (new Query())
                        ->select('es.siteId')
                        ->from('{{%elements_sites}} es')
                        ->where('[[es.elementId]] = [[e.id]]')
                        ->andWhere(['in', 'es.siteId', ArrayHelper::getColumn($allSiteSettings, 'siteId')])
                        ->limit(1)
                ])
                ->from(['{{%entries}} e'])
                ->where(['e.sectionId' => $section->id])
                ->orderBy(['e.id' => SORT_ASC])
                ->all();
        } else {
            $entryData = [];
        }

        // Get the section's entry types
        // ---------------------------------------------------------------------

        /** @var EntryType[] $entryTypes */
        $entryTypes = ArrayHelper::index($this->getEntryTypesBySectionId($section->id), 'id');

        if (empty($entryTypes)) {
            throw new Exception('Couldn’t find any entry types for the section: '.$section->id);
        }

        // Get/save the entry
        // ---------------------------------------------------------------------

        $entry = null;

        // If there are any existing entries, find the first one with a valid typeId
        foreach ($entryData as $data) {
            if (isset($entryTypes[$data['typeId']])) {
                $entry = Entry::find()
                    ->id($data['id'])
                    ->siteId($data['siteId'])
                    ->status(null)
                    ->enabledForSite(false)
                    ->one();
                break;
            }
        }

        // Otherwise create a new one
        if ($entry === null) {
            // Create one
            $firstSiteSettings = reset($allSiteSettings);
            $firstEntryType = reset($entryTypes);

            $entry = new Entry();
            $entry->siteId = $firstSiteSettings->siteId;
            $entry->sectionId = $section->id;
            $entry->typeId = $firstEntryType->id;
            $entry->title = $section->name;
        }

        // (Re)save it with an updated title, slug, and URI format.
        $entry->setScenario(Element::SCENARIO_ESSENTIALS);
        if (!Craft::$app->getElements()->saveElement($entry)) {
            throw new Exception('Couldn’t save single entry due to validation errors on the slug and/or URI');
        }

        // Delete any other entries in the section
        // ---------------------------------------------------------------------

        foreach ($entryData as $data) {
            if ($data['id'] != $entry->id) {
                Craft::$app->getElements()->deleteElementById($data['id'], Entry::class, $data['siteId']);
            }
        }

        // Delete any other entry types in the section
        // ---------------------------------------------------------------------

        foreach ($entryTypes as $entryType) {
            if ($entryType->id != $entry->typeId) {
                $this->deleteEntryType($entryType);
            }
        }

        // Update the remaining entry type's name and handle, if this isn't a new section
        // ---------------------------------------------------------------------

        if (!$isNewSection) {
            $entryTypes[$entry->typeId]->name = $section->name;
            $entryTypes[$entry->typeId]->handle = $section->handle;
            $this->saveEntryType($entryTypes[$entry->typeId]);
        }
    }

    /**
     * Performs some Structure-specific tasks when a section is saved.
     *
     * @param Section $section
     * @param bool $isNewSection
     * @param bool $isNewStructure
     * @param Section_SiteSettingsRecord[] $allOldSiteSettingsRecords
     * @see saveSection()
     * @throws Exception if reasons
     */
    private function _onSaveStructure(Section $section, bool $isNewSection, bool $isNewStructure, array $allOldSiteSettingsRecords)
    {
        if (!$isNewSection && $isNewStructure) {
            // Add all of the entries to the structure
            $query = Entry::find();
            /** @noinspection PhpUndefinedVariableInspection */
            $query->siteId(ArrayHelper::firstKey($allOldSiteSettingsRecords));
            $query->sectionId($section->id);
            $query->status(null);
            $query->enabledForSite(false);
            $query->orderBy('elements.id');
            $query->withStructure(false);
            /** @var Entry $entry */
            foreach ($query->each() as $entry) {
                Craft::$app->getStructures()->appendToRoot($section->structureId, $entry, 'insert');
            }
        }
    }

    /**
     * @return Query
     */
    private function _createEntryTypeQuery()
    {
        return (new Query())
            ->select([
                'id',
                'sectionId',
                'fieldLayoutId',
                'name',
                'handle',
                'hasTitleField',
                'titleLabel',
                'titleFormat',
            ])
            ->from(['{{%entrytypes}}']);
    }

}
